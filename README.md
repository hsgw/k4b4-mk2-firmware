# k4b4 mk2 bootloader #

This is a part of k4b4 mk2 tiny USB-MIDI controller.

* Tiny USB-MIDI controller with 4pot, 4buttons and 4LEDs.
* PIC18F25K50
* With USB-HID bootloader based on USB-HID bootloader in [Microchip Libraries for Application](http://www.microchip.com/pagehandler/en-us/devtools/mla/home.html)

## License ##

USB library : [Microchip Libraries for Application](http://www.microchip.com/pagehandler/en-us/devtools/mla/home.html)