#include <xc.h>
#include <stdbool.h>

#include "leds.h"

#define LED_D1_LAT LATCbits.LATC6
#define LED_D2_LAT LATCbits.LATC7
#define LED_D3_LAT LATCbits.LATC0
#define LED_D4_LAT LATCbits.LATC1

#define LED_D1_TRIS TRISCbits.TRISC6
#define LED_D2_TRIS TRISCbits.TRISC7
#define LED_D3_TRIS TRISCbits.TRISC0
#define LED_D4_TRIS TRISCbits.TRISC1

#define LED_ON  1
#define LED_OFF 0

#define PIN_INPUT           1
#define PIN_OUTPUT          0

#define PIN_DIGITAL         0
#define PIN_ANALOG          1


void LED_Initialize(void) {
    LED_D1_TRIS = PIN_OUTPUT;
    LED_D2_TRIS = PIN_OUTPUT;
    LED_D3_TRIS = PIN_OUTPUT;
    LED_D4_TRIS = PIN_OUTPUT;

    LED_D1_LAT = 0;
    LED_D2_LAT = 0;
    LED_D3_LAT = 0;
    LED_D4_LAT = 0;

}

void LED_Set(uint8_t no, bool state) {
    switch(no){
        case 0:
            LED_D1_LAT = state;
            break;
        case 1:
            LED_D2_LAT = state;
            break;
        case 2:
            LED_D3_LAT = state;
            break;
        case 3:
            LED_D4_LAT = state;
            break;
        default:break;
    }
}