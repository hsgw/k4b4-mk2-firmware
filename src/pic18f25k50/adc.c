#include <adc.h>
#include <stdint.h>
#include <stdbool.h>
#include <xc.h>

#include "adc.h"

#define ADC_POT1_TRIS  TRISBbits.TRISB1
#define ADC_POT1_ANSEL ANSELBbits.ANSB1
#define ADC_POT1_CH    10

#define ADC_POT2_TRIS  TRISBbits.TRISB0
#define ADC_POT2_ANSEL ANSELBbits.ANSB0
#define ADC_POT2_CH    12

#define ADC_POT3_TRIS  TRISAbits.TRISA5
#define ADC_POT3_ANSEL ANSELAbits.ANSA5
#define ADC_POT3_CH    4

#define ADC_POT4_TRIS  TRISCbits.TRISC2
#define ADC_POT4_ANSEL ANSELCbits.ANSC2
#define ADC_POT4_CH    14

#define PIN_ANALOG    1
#define PIN_DIGITAL   0

#define PIN_INPUT     1
#define PIN_OUTPUT    0


void ADC_Initialize(void){
    ADCON1=0;       // VREF+ to Vdd, VREF- to Vss
    ADCON2=0x2E;    // Result Left justified, ACQT 12TAD, Clock FOSC/64(1.3us)
    ADCON0=0x01;    // ADC enabled

    // POT1
    ADC_POT1_TRIS = PIN_INPUT;
    ADC_POT1_ANSEL = PIN_ANALOG;
    
    ADC_POT2_TRIS = PIN_INPUT;
    ADC_POT2_ANSEL = PIN_ANALOG;

    ADC_POT3_TRIS = PIN_INPUT;
    ADC_POT3_ANSEL = PIN_ANALOG;

    ADC_POT4_TRIS = PIN_INPUT;
    ADC_POT4_ANSEL = PIN_ANALOG;
}

uint8_t ADC_Read_8bits(enum ADC_NAME adcName){
    // select adc channel by name
    switch(adcName){
        case ADC_POT1:
            ADCON0bits.CHS = ADC_POT1_CH;
            break;
        case ADC_POT2:
            ADCON0bits.CHS = ADC_POT2_CH;
            break;
        case ADC_POT3:
            ADCON0bits.CHS = ADC_POT3_CH;
            break;
        case ADC_POT4:
            ADCON0bits.CHS = ADC_POT4_CH;
            break;
        default:return 0;
    }

    // start conversion
    ADCON0bits.GO = 1;
    // wait for conversion
    while(ADCON0bits.NOT_DONE);

    return ADRESH;
}

uint8_t ADC_Read_Normalize_7bits(enum ADC_NAME adcName) {
    uint16_t temp = 0;
    
    // select adc channel by name
    switch(adcName){
        case ADC_POT1:
            ADCON0bits.CHS = ADC_POT1_CH;
            break;
        case ADC_POT2:
            ADCON0bits.CHS = ADC_POT2_CH;
            break;
        case ADC_POT3:
            ADCON0bits.CHS = ADC_POT3_CH;
            break;
        case ADC_POT4:
            ADCON0bits.CHS = ADC_POT4_CH;
            break;
        default:return 0;
    }

    for(uint8_t i=0;i<32;i++){
        // start conversion
        ADCON0bits.GO = 1;
        // wait for conversion
        while(ADCON0bits.NOT_DONE);

        temp += ADRESH;
    }
    temp = temp >> 6;
    return temp;
}

