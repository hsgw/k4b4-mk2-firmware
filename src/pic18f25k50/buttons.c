#include <xc.h>
#include <stdbool.h>
#include "buttons.h"

/*** Button Definitions *********************************************/
#define S1_PORT  PORTBbits.RB2
#define S2_PORT  PORTBbits.RB4
#define S3_PORT  PORTBbits.RB6
#define S4_PORT  PORTBbits.RB7

#define S1_TRIS  TRISBbits.TRISB2
#define S2_TRIS  TRISBbits.TRISB4
#define S3_TRIS  TRISBbits.TRISB6
#define S4_TRIS  TRISBbits.TRISB7

#define S1_ANSEL ANSELBbits.ANSB2
#define S2_ANSEL ANSELBbits.ANSB4

#define S1_WPUB  WPUBbits.WPUB2
#define S2_WPUB  WPUBbits.WPUB4
#define S3_WPUB  WPUBbits.WPUB6
#define S4_WPUB  WPUBbits.WPUB7

#define S_ROW1_LAT LATAbits.LATA0
#define S_ROW2_LAT LATAbits.LATA1
#define S_ROW3_LAT LATAbits.LATA2
#define S_ROW4_LAT LATAbits.LATA3
#define S_ROW5_LAT LATAbits.LATA4

#define S_ROW1_TRIS TRISAbits.TRISA0
#define S_ROW2_TRIS TRISAbits.TRISA1
#define S_ROW3_TRIS TRISAbits.TRISA2
#define S_ROW4_TRIS TRISAbits.TRISA3
#define S_ROW5_TRIS TRISAbits.TRISA4


#define BUTTON_PRESSED      1

#define PIN_INPUT           1
#define PIN_OUTPUT          0

#define PIN_DIGITAL         0
#define PIN_ANALOG          1

#define PIN_PULLUP          1

void BUTTON_Initialize(void){
    //PORTB PULL-UP ON
    INTCON2bits.RBPU = 0;

    S1_ANSEL = PIN_DIGITAL;
    S2_ANSEL = PIN_DIGITAL;
//    S3_ANSEL = PIN_DIGITAL;
//    S4_ANSEL = PIN_DIGITAL;

    S1_TRIS = PIN_INPUT;
    S2_TRIS = PIN_INPUT;
    S3_TRIS = PIN_INPUT;
    S4_TRIS = PIN_INPUT;

    S1_WPUB = PIN_PULLUP;
    S2_WPUB = PIN_PULLUP;
    S3_WPUB = PIN_PULLUP;
    S4_WPUB = PIN_PULLUP;

    S_ROW1_TRIS = PIN_OUTPUT;
    S_ROW1_LAT = 0;

}

void BUTTON_Update(uint8_t *ret){
    ret[0] = S1_PORT;
    ret[1] = S2_PORT;
    ret[2] = S3_PORT;
    ret[3] = S4_PORT;
}

bool BUTTON_Get(uint8_t no){
    switch(no){
        case 0:
            return (S1_PORT == BUTTON_PRESSED);
        case 1:
            return (S2_PORT == BUTTON_PRESSED);
        case 2:
            return (S3_PORT == BUTTON_PRESSED);
        case 3:
            return (S4_PORT == BUTTON_PRESSED);
        default:return false;
    }
}
