#ifndef BUTTONS_H
#define BUTTONS_H

#include <stdbool.h>

void BUTTON_Initialize(void);

void BUTTON_Update(uint8_t *ret);

bool BUTTON_Get(uint8_t no);

#endif //BUTTONS_H
