#ifndef ADC_H
#define ADC_H

#include <stdint.h>

enum ADC_NAME {
    ADC_POT1 = 0,
    ADC_POT2 = 1,
    ADC_POT3 = 2,
    ADC_POT4 = 3
};

void ADC_Initialize(void);
uint8_t ADC_Read_8bits(enum ADC_NAME adcName);

uint8_t ADC_Read_Normalize_7bits(enum ADC_NAME adcName);

#endif  //ADC_H
