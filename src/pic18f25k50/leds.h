#ifndef LEDS_H
#define LEDS_H

#include <stdint.h>
#include <stdbool.h>

void LED_Initialize(void);

void LED_Set(uint8_t no, bool state);

#endif //LEDS_H
