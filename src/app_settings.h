/* 
 * File:   midi_setting.h
 * Author: urkwtky
 *
 * Created on 2015/07/18, 4:07
 */

#ifndef APP_SETTING_H
#define	APP_SETTING_H

// #define APP_DEV

// MIDI settings
#define MIDI_CH             0

#define MIDI_POT1_CC        13
#define MIDI_POT2_CC        14
#define MIDI_POT3_CC        15
#define MIDI_POT4_CC        16

#define MIDI_BUTTON1_NOTE   60
#define MIDI_BUTTON2_NOTE   61
#define MIDI_BUTTON3_NOTE   62
#define MIDI_BUTTON4_NOTE   63

// If you want to control LEDs by MIDI from PC, uncomment the line below.
// #define LED_CONTROL_BY_MIDI

#define MIDI_LED1_NOTE      60
#define MIDI_LED2_NOTE      61
#define MIDI_LED3_NOTE      62
#define MIDI_LED4_NOTE      63

#ifndef APP_DEV
#define BUTTON_COUNT        4
#else
#define BUTTON_COUNT        2
#endif

#define BUTTON_DEBOUNCE_TIME 20

#define LED_COUNT 4

#define POT_COUNT 4

#endif	/* APP_SETTING_H */

