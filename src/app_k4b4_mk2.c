#include <stdint.h>
#include <stdbool.h>

#include <system.h>

#include <buttons.h>
#include <leds.h>
#include <adc.h>

#include <usb.h>
#include <usb_device_midi.h>

#include "app_settings.h"


/** VARIABLES ******************************************************/
/* Some processors have a limited range of RAM addresses where the USB module
 * is able to access.  The following section is for those devices.  This section
 * assigns the buffers that need to be used by the USB module into those
 * specific areas.
 */
#if defined(FIXED_ADDRESS_MEMORY)
    #if defined(COMPILER_MPLAB_C18)
        #pragma udata DEVICE_AUDIO_MIDI_RX_DATA_BUFFER=DEVCE_AUDIO_MIDI_RX_DATA_BUFFER_ADDRESS
            static uint8_t ReceivedDataBuffer[64];
        #pragma udata DEVICE_AUDIO_MIDI_EVENT_DATA_BUFFER=DEVCE_AUDIO_MIDI_EVENT_DATA_BUFFER_ADDRESS
            static USB_AUDIO_MIDI_EVENT_PACKET midiData;
        #pragma udata
    #elif defined(__XC8)
        static uint8_t ReceivedDataBuffer[64] @ DEVCE_AUDIO_MIDI_RX_DATA_BUFFER_ADDRESS;
        static USB_AUDIO_MIDI_EVENT_PACKET midiData @ DEVCE_AUDIO_MIDI_EVENT_DATA_BUFFER_ADDRESS;
    #endif
#else
    static uint8_t ReceivedDataBuffer[64];
    static USB_AUDIO_MIDI_EVENT_PACKET midiData;
#endif

static const uint8_t MIDI_NOTES[BUTTON_COUNT] = {MIDI_BUTTON1_NOTE,MIDI_BUTTON2_NOTE,MIDI_BUTTON3_NOTE,MIDI_BUTTON4_NOTE};
static const uint8_t MIDI_CCNO[POT_COUNT] = {MIDI_POT1_CC, MIDI_POT2_CC, MIDI_POT3_CC, MIDI_POT4_CC};

#ifdef LED_CONTROL_BY_MIDI
static const uint8_t MIDI_LED_NOTES[LED_COUNT] = {MIDI_LED1_NOTE,MIDI_LED2_NOTE,MIDI_LED3_NOTE,MIDI_LED4_NOTE};
static USB_AUDIO_MIDI_EVENT_PACKET* usbMidiReceivedPacket;
#endif

static USB_HANDLE USBTxHandle;
static USB_HANDLE USBRxHandle;

static uint8_t currentButtons[BUTTON_COUNT];
static uint8_t prevButtons[BUTTON_COUNT];
static USB_VOLATILE uint8_t buttonDebounceCount[BUTTON_COUNT];

static uint8_t prevPotValues[POT_COUNT];
static uint8_t prevCCValues[POT_COUNT];

void delay_ms(unsigned long time){
    for(unsigned long i=0;i<time;i++){
        _delay(48000); // delay 1ms
    }
}

void sendMIDINoteON(uint8_t pitch, uint8_t velocity) {
     if(!USBHandleBusy(USBTxHandle)) {
            midiData.Val = 0;   //must set all unused values to 0 so go ahead
                                //  and set them all to 0
            midiData.CableNumber = 0;
            midiData.CodeIndexNumber = MIDI_CIN_NOTE_ON;
            midiData.DATA_0 = 0x90 | MIDI_CH;       //Note on
            midiData.DATA_1 = pitch;                //pitch
            midiData.DATA_2 = velocity;             //velocity

            USBTxHandle = USBTxOnePacket(USB_DEVICE_AUDIO_MIDI_ENDPOINT,(uint8_t*)&midiData,4);
        }
}

void sendMIDINoteOFF(uint8_t pitch) {
     if(!USBHandleBusy(USBTxHandle)) {
            midiData.Val = 0;   //must set all unused values to 0 so go ahead
                                //  and set them all to 0
            midiData.CableNumber = 0;
            midiData.CodeIndexNumber = MIDI_CIN_NOTE_OFF;
            midiData.DATA_0 = 0x80 | MIDI_CH;  //Note off
            midiData.DATA_1 = pitch;           //pitch
            midiData.DATA_2 = 0x7F;            //velocity

            USBTxHandle = USBTxOnePacket(USB_DEVICE_AUDIO_MIDI_ENDPOINT,(uint8_t*)&midiData,4);
        }
}


void sendMIDIControlChange(uint8_t cc, uint8_t value) {
     if(!USBHandleBusy(USBTxHandle)) {
            midiData.Val = 0;   // must set all unused values to 0 so go ahead
                                // and set them all to 0
            midiData.CableNumber = 0;
            midiData.CodeIndexNumber = MIDI_CIN_CONTROL_CHANGE;
            midiData.DATA_0 = 0xB0 | MIDI_CH;  // control change
            midiData.DATA_1 = cc;              // cc no
            midiData.DATA_2 = value;           // value

            USBTxHandle = USBTxOnePacket(USB_DEVICE_AUDIO_MIDI_ENDPOINT,(uint8_t*)&midiData,4);
        }
}


void APP_DeviceAudioMIDIInitialize()
{
    USBTxHandle = NULL;
    USBRxHandle = NULL;
    
    for(uint8_t i=0;i<BUTTON_COUNT;i++){
        currentButtons[i] = 1;
        prevButtons[i] = 1;
        buttonDebounceCount[i] = 0;
    }
    
    for(uint8_t i=0;i<POT_COUNT;i++){
        prevPotValues[i] = ADC_Read_8bits(i)>>1;
        prevCCValues[i] = prevPotValues[i];
    }
    
    LED_Initialize();
    BUTTON_Initialize();
    ADC_Initialize();
    
//    for(uint8_t i=0;i<LED_COUNT;i++){
//        LED_Set(i, true);
//    }
//    //250ms delay
//    delay_ms(500);
//    for(uint8_t i=0;i<LED_COUNT;i++){
//        LED_Set(i, false);
//    }

    //enable the HID endpoint
    USBEnableEndpoint(USB_DEVICE_AUDIO_MIDI_ENDPOINT,USB_OUT_ENABLED|USB_IN_ENABLED|USB_HANDSHAKE_ENABLED|USB_DISALLOW_SETUP);

    //Re-arm the OUT endpoint for the next packet
    USBRxHandle = USBRxOnePacket(USB_DEVICE_AUDIO_MIDI_ENDPOINT,(uint8_t*)&ReceivedDataBuffer,64);

}


void APP_DeviceAudioMIDISOFHandler()
{
    for(uint8_t i=0;i<BUTTON_COUNT;i++){
        if(buttonDebounceCount[i] != 0){
            buttonDebounceCount[i]--;
        }
    }
}


void APP_DeviceAudioMIDITasks()
{
    uint16_t tempAdc;
    /* If the device is not configured yet, or the device is suspended, then
     * we don't need to run the demo since we can't send any data.
     */
    if( (USBGetDeviceState() < CONFIGURED_STATE) ||
        (USBIsDeviceSuspended() == true))
    {
        return;
    }

    if(!USBHandleBusy(USBRxHandle))
    {
        //We have received a MIDI packet from the host, process it and then
        //  prepare to receive the next packet

    #ifdef LED_CONTROL_BY_MIDI
        usbMidiReceivedPacket = (USB_AUDIO_MIDI_EVENT_PACKET*)&ReceivedDataBuffer;
        
        if(usbMidiReceivedPacket->DATA_0 == 0x90 | MIDI_CH){
            // recieved Note ON
            for(uint8_t i=0;i<LED_COUNT;i++){
                if(usbMidiReceivedPacket->DATA_1 == MIDI_LED_NOTES[i]){
                    LED_Set(i,true);
                    break;
                }
            }

        }else if(usbMidiReceivedPacket->DATA_0 == 0x80 | MIDI_CH){
            // recieved Note OFF
            for(uint8_t i=0;i<LED_COUNT;i++){
                if(usbMidiReceivedPacket->DATA_1 == MIDI_LED_NOTES[i]){
                    LED_Set(i,false);
                    break;
                }
            }
        }
    #endif

        //INSERT MIDI PROCESSING CODE HERE
        //Get ready for next packet (this will overwrite the old data)
        USBRxHandle = USBRxOnePacket(USB_DEVICE_AUDIO_MIDI_ENDPOINT,(uint8_t*)&ReceivedDataBuffer,64);
    }

    // read buttons and send midi note
    BUTTON_Update(currentButtons);
    for(uint8_t i=0;i<BUTTON_COUNT;i++){
        #ifndef LED_CONTROL_BY_MIDI
        LED_Set(i,!currentButtons[i]);
        #endif
        if(buttonDebounceCount[i] == 0){
            if(currentButtons[i] != prevButtons[i]){
                buttonDebounceCount[i] = BUTTON_DEBOUNCE_TIME;
                prevButtons[i] = currentButtons[i];
                if(currentButtons[i] == 0){
                    sendMIDINoteON(MIDI_NOTES[i], 0x7F);
                }else{
                    sendMIDINoteOFF(MIDI_NOTES[i]);
                }
            }
        }
    }

    for(uint8_t i=0;i<POT_COUNT;i++){
        // read potentiometer
        tempAdc = ADC_Read_Normalize_7bits(i);

        if(tempAdc == prevPotValues[i]){
            prevPotValues[i] = tempAdc;
        }else{
            prevPotValues[i] = tempAdc;
            tempAdc = (tempAdc * 7 + prevCCValues[i]) >> 3;
        }

        if(tempAdc != prevCCValues[i]){
            prevCCValues[i] = tempAdc;
            sendMIDIControlChange(MIDI_CCNO[i], (uint8_t)tempAdc);
        }
    }
}
