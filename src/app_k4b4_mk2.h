
#ifndef APP_K4B4_MK2_H
#define APP_K4B4_MK2_H

void APP_DeviceAudioMIDIInitialize();

void APP_DeviceAudioMIDITasks();

void APP_DeviceAudioMIDISOFHandler();

#endif